# AS_TurtleRace_Simulator

Welcome on the TurtleRace Simulator repository !

In this repository you will find everything you need to start the simulation environment for the TurtleRace Project.

## Getting started

Look at the [wiki](https://gitlab.com/as_turtlerace/as_turtlerace_simulator/-/wikis/home) to set up the simulation environment.
