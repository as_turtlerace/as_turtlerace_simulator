# Installation script for the turtlebot simulation environment

sudo apt-get install -y ros-melodic-ecl
sudo apt-get install -y ros-melodic-joy
sudo apt-get install -y ros-melodic-kobuki-core
sudo apt-get install -y ros-melodic-kobuki-msgs
sudo apt-get install -y ros-melodic-yocs-controllers
sudo apt-get install -y ros-melodic-yocs-msgs
sudo apt-get install -y ros-melodic-ar-track-alvar
sudo apt-get install -y ros-melodic-depthimage-to-laserscan
sudo apt-get install -y pyqt5-dev-tools

sudo apt install -y ros-melodic-moveit-core
sudo apt install -y ros-melodic-moveit-ros-planning-interface

sudo apt install -y ros-melodic-slam-gmapping

sudo apt-get install -y ros-melodic-dwa-local-planner
sudo apt-get install -y ros-melodic-amcl
sudo apt-get install -y ros-melodic-map-server
sudo apt-get install -y ros-melodic-move-base
sudo apt-get install -y ros-melodic-move-base-msgs
sudo apt-get install -y ros-melodic-base-local-planner


mkdir -p turtlerace_ws/src

cd turtlerace_ws
catkin_make

cd src

git clone https://github.com/turtlebot/turtlebot.git
git clone https://github.com/turtlebot/turtlebot_msgs.git
git clone https://github.com/turtlebot/turtlebot_apps.git

mv turtlebot_apps/turtlebot_navigation .
rm -fr turtlebot_apps   ##  action compilation error. Only need to navigate

git clone https://github.com/turtlebot/turtlebot_interactions.git
git clone https://github.com/turtlebot/turtlebot_simulator.git -b melodic
git clone https://github.com/turtlebot/turtlebot_arm.git -b melodic-devel
git clone https://github.com/yujinrobot/kobuki_msgs.git -b melodic 
git clone https://github.com/yujinrobot/kobuki.git -b melodic 
git clone https://github.com/yujinrobot/yujin_ocs.git
git clone https://github.com/yujinrobot/kobuki_desktop.git

cd ..
catkin_make
